<?php
header("Content-Type: text/html; charset=utf-8");
$csv = array_map('str_getcsv', file('searches.csv'));
$arr = [];
$pattern = '/^\[([^\]]+)\]$/';
for ($i=1;$i<count($csv);$i++) {
    $row = $csv[$i];
    $keyword = $row[0];
    $searches = $row[1];
    $reg_words = preg_match($pattern,$keyword,$matches);
    if ($reg_words) {
        $keyword = $matches[1];
    }
    $arr_keywords = explode(' ',$keyword);
    foreach ($arr_keywords as $word) {
        $hash = sha1($word);
        if ($reg_words) {
            if (isset($arr[$hash]['total_broads_src'])) {
                $total_broads_src = $arr[$hash]['total_broads_src']+$searches;    
            } else {
                $total_broads_src = $searches;
            }
            $arr[$hash]['total_broads_src'] =  $total_broads_src;
        } else {
            if (isset($arr[$hash]['total_extract_src'])) {
                $total_extract_src = $arr[$hash]['total_extract_src']+$searches;
            } else {
                $total_extract_src = $searches;
            }
            $arr[$hash]['total_extract_src'] =  $total_extract_src;
        }
        $cnt = isset($arr[$hash]['count']) ? $arr[$hash]['count'] + 1 : 1;
        $arr[$hash]['word'] = $word;
        $arr[$hash]['count'] = $cnt;
    }
}

echo 'Word,Count,Total Broad Searches,Total Exact Searches'."\r\n";

foreach ($arr as $row) {
    echo $row['word'].','.
        $row['count'].','.
        $row['total_broads_src'].','.
        $row['total_extract_src']."\r\n";
}
